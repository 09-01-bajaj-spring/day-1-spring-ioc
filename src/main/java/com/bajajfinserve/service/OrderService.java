package com.bajajfinserve.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bajajfinserve.model.Order;
import com.bajajfinserve.repository.OrderJpaRepository;

@Service
public class OrderService {
	
	private final OrderJpaRepository orderRepository;
	
	public OrderService(OrderJpaRepository orderRepository) {
		this.orderRepository = orderRepository;
	}
	
	public Order save(Order order) {
		return this.orderRepository.save(order);
	}
	
	public List<Order> fetchAll(){
		return this.orderRepository.findAll();
	}
	
	public void deleteOrder(long id) {
		this.orderRepository.deleteById(id);
	}
	
	public Order fetchOrderById(long id) {
		return this.orderRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("invalid order id passed"));
	}

}
