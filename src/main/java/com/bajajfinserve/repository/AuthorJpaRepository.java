package com.bajajfinserve.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bajajfinserve.model.Author;

@Repository
public interface AuthorJpaRepository extends JpaRepository<Author, Integer>{

}
