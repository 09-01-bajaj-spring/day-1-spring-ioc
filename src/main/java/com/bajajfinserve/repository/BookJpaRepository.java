package com.bajajfinserve.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bajajfinserve.model.Book;

@Repository
public interface BookJpaRepository extends JpaRepository<Book, Integer>{

}
