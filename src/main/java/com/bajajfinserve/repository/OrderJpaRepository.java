package com.bajajfinserve.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bajajfinserve.model.Order;

@Repository
public interface OrderJpaRepository extends JpaRepository<Order, Long>{
	
}
