package com.bajajfinserve.client;

import java.util.stream.Stream;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bajajfinserve.config.Passenger;

public class ApplicationContextClient {
	
	public static void main(String[] args) {
		
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
		
		/*
		 * String[] beanNames = applicationContext.getBeanDefinitionNames();
		 * Stream.of(beanNames).forEach(System.out::println);
		 */
		
		Passenger passenger1 = applicationContext.getBean("passenger", Passenger.class);
		Passenger passenger2 = applicationContext.getBean("passenger", Passenger.class);
		
		passenger1.travel("BTM-Layout", "Whitefield");
		
		System.out.println(passenger1 == passenger2);

		
		((AbstractApplicationContext)applicationContext).close();
	}

}
