package com.bajajfinserve.client;

import java.util.stream.Stream;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bajajfinserve.config.AppConfig;

public class Client {
	
	public static void main(String[] args) {
		/*
		 * Commute uberGo = new UberGo(); Passenger ravi = new Passenger(uberGo);
		 */
		
		//ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
		
		//ApplicationContext annotationApplicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
		
		//Stream.of(annotationApplicationContext.getBeanDefinitionNames()).forEach(System.out::println);
		
		/*
		 * Passenger ravi = applicationContext.getBean("passenger", Passenger.class);
		 * 
		 * ravi.travel("Hingewadi", "Wakad");
		 * 
		 * 
		 * ravi.getDriverDetails().printDriverDetails();
		 * 
		 * 
		 * ((AbstractApplicationContext)applicationContext).close();
		 */
		
	}

}
