package com.bajajfinserve.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bajajfinserve.model.Author;
import com.bajajfinserve.model.Book;
import com.bajajfinserve.repository.AuthorJpaRepository;
import com.bajajfinserve.repository.BookJpaRepository;

public class JpaManyToManyClient {
	
	public static void main(String[] args) {
		
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
		
		
		Author vinay = new Author("Vinay", "vinay@gmail.com");
		Author ravish = new Author("Ravish", "ravish@gmail.com");
		
		Book book1 = new Book("book-1", 500);
		Book book2 = new Book("book-2", 600);
		Book book3 = new Book("book-3", 800);
		Book book4 = new Book("book-4", 900);
		Book book5 = new Book("book-5", 800);
		Book book6 = new Book("book-6", 900);
		
		vinay.addBook(book1);
		vinay.addBook(book2);
		
		ravish.addBook(book1);
		ravish.addBook(book2);
		ravish.addBook(book3);
		ravish.addBook(book4);
		ravish.addBook(book5);
		ravish.addBook(book6);
		
		AuthorJpaRepository authorJpaRepository = applicationContext.getBean("authorJpaRepository", AuthorJpaRepository.class);
		BookJpaRepository bookJpaRepository = applicationContext.getBean("bookJpaRepository", BookJpaRepository.class);
		
		authorJpaRepository.save(vinay);
		authorJpaRepository.save(ravish);
		
		bookJpaRepository.save(book1);
		bookJpaRepository.save(book2);
		bookJpaRepository.save(book3);
		bookJpaRepository.save(book4);
		bookJpaRepository.save(book5);
		bookJpaRepository.save(book6);
		
		
		
	}

}
