package com.bajajfinserve.client;

import java.time.LocalDate;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bajajfinserve.model.LineItem;
import com.bajajfinserve.model.Order;
import com.bajajfinserve.service.OrderService;

public class JpaClient {
	
	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
		OrderService orderService = applicationContext.getBean("orderService", OrderService.class);
		
		Order order1 = new Order("Ravish", "ravish@gmail.com", LocalDate.now());
		/*
		 * Order order2 = new Order("Vinay", "vinay@gmail.com", LocalDate.now()); Order
		 * order3 = new Order("Akash", "aksah@gmail.com", LocalDate.now()); Order order4
		 * = new Order("Vikaram", "vikram@gmail.com", LocalDate.now());
		 */
		
		LineItem lineItem1 = new LineItem(4000, 2 ,"Levis");
		LineItem lineItem2 = new LineItem(400, 2, "Parker-Pen");
		LineItem lineItem3 = new LineItem(500, 1, "Leather-Belt");
		
		order1.addLineItem(lineItem1);
		order1.addLineItem(lineItem2);
		order1.addLineItem(lineItem3);
		
		orderService.save(order1);
		/*
		 * orderService.save(order2); orderService.save(order3);
		 * orderService.save(order4);
		 */
		/*
		 * List<Order> orders = orderService.fetchAll();
		 * 
		 * orders.stream().forEach(order -> System.out.println(order.getName()));
		 * 
		 * Order fetchedOrder = orderService.fetchOrderById(1);
		 * System.out.println("Fetched order by id :: "+ fetchedOrder);
		 * 
		 * orderService.deleteOrder(1L);
		 */
	}

}
