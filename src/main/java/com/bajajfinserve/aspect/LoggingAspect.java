package com.bajajfinserve.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class LoggingAspect {

	@Before("execution(* com.bajajfinserve.config.Passenger.travel(..))")
	public void logBefore(JoinPoint joinPoint) {
		System.out.println("logging before the method is getting executed----");
		System.out.println(joinPoint);
	}

	@After("execution(* com.bajajfinserve.config.Passenger.*(..))")
	public void logAfter(JoinPoint joinPoint) {
		System.out.println("logging after the method has executed:::: ");
	}

}
