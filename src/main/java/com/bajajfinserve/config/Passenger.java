package com.bajajfinserve.config;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class Passenger {
	
	private Commute commute;
	
	@PostConstruct
	public void customInit() {
		System.out.println("Custom initialization method for Passenger::::");
	}
	
	public Passenger(@Qualifier("uberGo") Commute commute) {
		this.commute = commute;
	}
	
	public void travel(String from, String to) {
		this.commute.trip(from, to);
	}
	
	public Commute getDriverDetails() {
		return this.commute;
	}
	
	@PreDestroy
	public void customDestroy() {
		System.out.println("before destryoing the object:::: ");
	}

}
