package com.bajajfinserve.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
public class UberGo implements Commute {
	
	@Value("Ravish")
	private String name;
	
	@Value("KA-41-MG-7877")
	private String vehicleNumber;
	
	
	
	
	public String getName() {
		return this.name;
	}
	
	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}


	public void trip(String from, String destination) {
		System.out.println("Travelling from "+ from + " to destination :: "+ destination + " with Uber Go!!");
	}


	public void printDriverDetails() {
		System.out.println("Driver name is "+ name + "vehicle number is "+ vehicleNumber);
	}
}
