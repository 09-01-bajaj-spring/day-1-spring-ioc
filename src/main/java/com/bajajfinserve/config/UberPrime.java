package com.bajajfinserve.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class UberPrime implements Commute {
	
	@Value("Nitesh")
	private String name;
	
	private String vehicleNumber;
	
	public String getName() {
		return this.name;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public void trip(String from, String destination) {
		System.out.println("Travelling from "+ from + " to destination :: "+ destination + " with Uber Prime!!");
	}

	public void printDriverDetails() {
		System.out.println("Driver name is "+ name + "vehicle number is "+ vehicleNumber);
		
	}
}

