package com.bajajfinserve.config;

public interface Commute {
	
	void trip(String from, String destination);
	
	void printDriverDetails();

}
